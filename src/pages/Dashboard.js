import React from 'react'
import Bar from '../components/Bar'

const Dashboard = () => {
  return (
    <div>
        <Bar/>
        <h2>
            <a href='/landing'>Link to Landing page</a>
        </h2>
    </div>
  )
}

export default Dashboard